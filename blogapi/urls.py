from django.urls import path, re_path
from blogapi.views import login, article, comment, mood, user, file, apiEmail, history, sites

app_name = 'blogapi'
urlpatterns = [
    path('login', login.LoginView.as_view(), name='login'),  # 登录
    path('sign', login.SignView.as_view(), name='sign'),  # 注册
    path('article', article.ArticleView.as_view(), name='article'),  # 发布文章
    path('article/<int:nid>', article.ArticleView.as_view(), name='article'),  # 修改文章
    # 查看加密文章
    path('articlePwd/<int:nid>', article.ArticlePwdView.as_view(), name='articlePwd'),
    re_path(r'article/comment/(?P<nid>\d+)', comment.CommentView.as_view(), name='comment'),  # 发布评论
    # 评论点赞
    re_path('comment/digg/(?P<nid>\d+)', comment.CommentDiggView.as_view(), name='comment_digg'),
    # 文章点赞
    re_path('article/digg/(?P<nid>\d+)', article.ArticleDiggView.as_view(), name='article_digg'),
    # 文章收藏
    re_path('article/collect/(?P<nid>\d+)', article.ArticleCollectsView.as_view(), name='article_collect'),
    # 文章封面修改
    path('article/cover/<int:nid>', article.EditArticleCoverView.as_view(), name='EditArticleCover'),
    # 心情路由
    path('mood', mood.MoodView.as_view(), name='mood'),
    re_path(r'mood/(?P<nid>\d+)', mood.MoodView.as_view(), name='mood_delete'),  # 删除心情
    re_path(r'mood_comment/(?P<nid>\d+)', mood.MoodCommentView.as_view(), name='mood_comment_edit'),  # 修改心情
    # 修改密码
    path('edit_password', user.EditPasswordView.as_view(), name='editPassword'),
    # 修改头像
    path('change_avatar', user.EditAvatarView.as_view(), name='editAvatar'),
    path('upload/avatar', file.AvatarUploadView.as_view(), name='AvatarUpload'),
    path('upload/avatar/<int:nid>', file.AvatarUploadView.as_view(), name='AvatarDelete'),
    path('upload/cover', file.CoverUploadView.as_view(), name='CoverUpload'),
    path('upload/cover/<int:nid>', file.CoverUploadView.as_view(), name='CoverDelete'),
    # 粘贴上传
    path('paste_upload', file.PasteUploadView.as_view(), name='pasteUpload'),
    path('sendEmail', apiEmail.ApiEmailView.as_view(), name='sendEmail'),
    path('perfectInformation', user.EditUserInfoView.as_view(), name='perfectInformation'),
    path('cancelCollection', user.CancelCollectionView.as_view(), name='cancelCollection'),  # 取消收藏
    # 回忆录
    # 发布记录
    path('history/', history.HistoryView.as_view(), name='history'),
    # 编辑记录
    path('history/<int:nid>', history.HistoryView.as_view(), name='history'),
    # 网站导航
    # 添加网站标签
    path('addSiteTag/', sites.NavTagsView.as_view(), name='addSiteTag'),
    path('addSiteTag/<int:nid>', sites.NavTagsView.as_view(), name='EditOrDeleteSiteTag'),
    # 获取网站数据列表
    path('sites/', sites.NavView.as_view(), name='sites'),
    # 编辑网站数据
    path('sites/<int:nid>', sites.NavView.as_view(), name='EditSites'),
    # 用于动态获取网站标签
    path('siteTag', sites.NavTagView.as_view(), name='SiteTag'),
    # 网站点赞
    path('siteDigg/<int:nid>', sites.NavDiggView.as_view(), name='SiteDigg'),
    # 网站收藏
    path('siteCollect/<int:nid>', sites.NavCollectsView.as_view(), name='siteCollect'),
    # 意见反馈
    path('feedback', user.FeedBackView.as_view(), name='feedback'),
]
