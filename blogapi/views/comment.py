from django.views import View
from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.db.models import F
from django import forms
from blogapi.views.login import clean_form
from blog01.models import Comment, Articles
from blogapi.utils.comment_find import find_root_comment
from blog01.utils.subcomment import find_root_subcomment


class CommentView(View):
    # 发布评论
    def post(self, request, nid):
        '''
        文章id 用户 评论内容 必填
        /api/article/2/comment
        :param request:
        :return:
        '''
        res = {
            'msg': '文章评论成功',
            'code': 0,
            'self': None
        }
        data = request.data
        if not request.user.username:
            res['msg'] = '请登录'
            res['code'] = -1
            return JsonResponse(res)
        content = data.get('content')
        if not content:
            res['msg'] = '请输入评论内容'
            res['code'] = -1
            res['self'] = 'content'
            return JsonResponse(res)
        pid = data.get('pid')
        # 文章评论量加1
        Articles.objects.filter(nid=nid).update(comment_count=F('comment_count') + 1)
        if pid:
            # 文章评论校验成功
            # 不是根评论
            comment_obj = Comment.objects.create(
                content=content,
                user=request.user,
                article_id=nid,
                parent_comment_id=pid,
            )
            # 根评论数+1
            # 找最终的根评论
            root_comment_obj = find_root_comment(comment_obj)
            root_comment_obj.comment_count += 1
            root_comment_obj.save()
            # Comment.objects.filter(nid=pid).update(comment_count=F('comment_count')+1)
        else:
            # 根评论
            Comment.objects.create(
                content=content,
                user=request.user,
                article_id=nid,
            )
        return JsonResponse(res)

    def delete(self, request, nid):
        # 自己发布的评论才能删除
        # 或者是超级管理员
        res = {
            'msg': '评论删除成功',
            'code': 0,
        }
        # 超级管理员
        login_user = request.user
        comment_query = Comment.objects.filter(nid=nid)
        # 评论人
        comment_user = comment_query.first().user
        aid = request.data.get('aid')
        # 子评论的最终根评论id
        pid = request.data.get('pid')
        if login_user == comment_user or login_user.is_superuser:
            # 可以删除
            lis = []
            find_root_subcomment(comment_query.first(), lis)
            # 子评论+自己
            Articles.objects.filter(nid=aid).update(comment_count=F('comment_count') - len(lis) - 1)
            # 文章评论数-1
            if pid:
                # 找最终的根评论 - 1
                Comment.objects.filter(nid=pid).update(comment_count=F('comment_count') - len(lis) - 1)
            comment_query.delete()
        else:
            res['msg'] = '用户验证失败'
            res['code'] = -1
        return JsonResponse(res)


class CommentDiggView(View):
    def post(self, request, nid):
        # nid 文章评论id
        res = {
            'msg': '文章评论点赞成功',
            'code': 0,
            'data': 0,
        }
        # data = request.data
        if not request.user.username:
            res['msg'] = '请登录'
            res['code'] = -1
            return JsonResponse(res)
        comment_query = Comment.objects.filter(nid=nid)
        comment_query.update(digg_count=F('digg_count') + 1)
        res['data'] = comment_query.first().digg_count
        return JsonResponse(res)
