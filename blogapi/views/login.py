from django import forms
from django.contrib import auth
from blog01.models import UserInfo, Avatars
from django.views import View
from django.shortcuts import render
from django.http import JsonResponse

import random

class LoginBaseForm(forms.Form):
    name = forms.CharField(error_messages={'required': '请输入用户名'})
    pwd = forms.CharField(error_messages={'required': '请输入密码'})
    code = forms.CharField(error_messages={'required': '请输入验证码'})

    # 重写init方法
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super().__init__(*args, **kwargs)

    # 局部钩子
    def clean_code(self):
        code: str = self.cleaned_data.get('code')
        verifycode: str = self.request.session.get('verify_code')
        if code.upper() != verifycode.upper():
            self.add_error('code', '验证码输入错误')
        return self.cleaned_data


# 登录字段验证
class LoginForm(LoginBaseForm):
    # 全局钩子
    def clean(self):
        name = self.cleaned_data.get('name')
        pwd = self.cleaned_data.get('pwd')
        user = auth.authenticate(username=name, password=pwd)
        error_count = self.request.session.get('error_count', 0)
        if error_count >= 3:
            self.add_error('name', '错误过多，请重新获取验证码')
            return None
        if not user:
            # 为某一个字段添加一条错误信息
            self.add_error('pwd', '用户名或密码错误')
            error_count += 1
            self.request.session['error_count'] = error_count
            return self.cleaned_data

        # 把用户对象放到cleaned_data里面
        self.cleaned_data['user'] = user
        return self.cleaned_data


# 注册字段验证
class SignForm(LoginBaseForm):
    repwd = forms.CharField(error_messages={'required': '请输入确认密码'})

    def clean(self):
        # 验证密码
        pwd = self.cleaned_data.get('pwd')
        repwd = self.cleaned_data.get('repwd')
        if pwd != repwd:
            self.add_error('repwd', '两次密码不一致')
            return self.cleaned_data

    def clean_name(self):
        name = self.cleaned_data.get('name')
        print('验证用户名为'+name)
        userquery = UserInfo.objects.filter(username=name)
        if userquery:
            print('用户名出错')
            self.add_error('name', '用户名已注册')
        return self.cleaned_data

# 登录失败可复用代码
def clean_form(form):
    # 验证不通过
    err_dict: dict = form.errors
    # 拿到所有错误字段名字
    err_valid = list(err_dict.keys())[0]
    # 拿到第一个字段的第一个错误信息
    # print(err_dict[err_valid], type(err_dict[err_valid]))
    err_msg = err_dict[err_valid][0]
    # print(err_valid, err_msg)
    return err_valid, err_msg


class LoginView(View):

    def get(self, request):
        return render(request, 'blog01/login.html')

    def post(self, request):
        # 返回给前端的一个数据格式 针对登录页面
        res = {
            'code': 0,  # 返回0表示成功
            'msg': '登录成功',  # 返回一个提示信息
            'self': None,  # 提示哪个字段提示错误
        }
        # verify_code:str = request.session['verify_code']
        data = request.data
        form = LoginForm(data, request=request)
        if not form.is_valid():
            err_valid, err_msg = clean_form(form)
            res['msg'] = err_msg
            res['self'] = err_valid
            res['code'] = -1
            return JsonResponse(res)
        # 登录操作
        user = form.cleaned_data.get('user')
        auth.login(request, user)
        return JsonResponse(res)


class SignView(View):
    def get(self, request):
        return render(request, 'blog01/sign.html')

    def post(self, request):
        res = {
            'code': 0,  # 返回0表示成功
            'msg': '注册成功',  # 返回一个提示信息
            'self': None,  # 提示哪个字段提示错误
        }
        form = SignForm(request.data, request=request)
        if not form.is_valid():
            # 验证不通过
            res['code'] = -1
            res['self'], res['msg'] = clean_form(form)
            return JsonResponse(res)
        # 注册成功的代码
        user = UserInfo.objects.create_user(
            username=request.data.get('name'),
            password=request.data.get('pwd'),
        )
        # 随机选择头像
        avatar_list = [i.nid for i in Avatars.objects.all()]
        user.avatar_id = random.choice(avatar_list)
        user.save()
        # 注册之后 直接登录
        auth.login(request, user)
        return JsonResponse(res)