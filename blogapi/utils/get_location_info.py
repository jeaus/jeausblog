import requests
import re
from bs4 import BeautifulSoup

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36"
}


def get_ip(request):
    # 判断是否使用代理
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        # 代理
        ip = x_forwarded_for.split(',')[0]
    else:
        # 获取真实ip
        ip = request.META.get('REMOTE_ADDR')
    return ip


def get_addr_ip(ip):
    url = f'https://www.ip138.com/iplookup.asp?ip={ip}&action=2'
    res = requests.get(url=url, headers=headers).content
    html = res.decode('gbk')
    soup = BeautifulSoup(html, 'html.parser')
    script = soup.findAll(name='script', attrs={'type': 'text/javascript'})
    text = script[1].string
    result = eval(re.findall(r'ip_result = (.*?);', text, re.S)[0])['ip_c_list'][0]
    # 内网地址会在ct里面直接显示保留地址
    return result



if __name__ == '__main__':
    get_addr_ip('171.42.143.203')
    get_addr_ip('127.0.0.1')
