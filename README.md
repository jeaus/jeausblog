# 记录过程中的一些小坑和学习笔记

## 1. SCSS文件修改之后，并且文件中的css也修改了，但是页面没效果
1. 查看SCSS文件是否有错误
2. Ctrl+F5 重新刷新试下

## 2. 伪类和伪元素的区别
1. 本质在于有没有创建文档树之外的元素
    伪类:自己存在，但是没有添加各种名字，可以通过css来修饰其自己，比如通过:hover之类的
    伪元素:自己不存在，但是在DOM树上面创建了一些不存在的元素，然后给其添加样式

常见伪类:
```css
    :hover 选择被鼠标悬浮着的元素（匹配指定状态）
    :first-child 选择满足是其父元素的第一个子元素的元素
    :focus 选择拥有键盘输入焦点的元素
    :checked 选择每个被选中的元素
    :disable 选择每个已禁止的元素
    :nth-of-type(n) 选择满足是其父元素的第n个某类型子元素的元素
```

常用伪元素
```css
    ::first-letter 选择指定元素的第一个单词
    ::first-line 选择指定元素的第一行
    ::after 在指定元素的内容前面插入内容
    ::before 在指定元素的内容后面插入内容
    ::selection 选择指定元素中被用户选中的内容
```

DOM树
DOM（Document Object Model）即文档对象模型, 包含HTML页面中的各种标签

## 盒子模型
HTML中的元素都会有margin，padding，border,content的值，一般设置width/height的时候
通常指定的是content的,可以通过box-sizing来设置width是否包含margin，padding的值,border-box为包含,content-box不包含

CSS中盒模型分为两种：W3C标准和IE标准盒子模型
    1. 标准盒子模型
        padding,border所占的空间不在width,height范围中，width=content的宽。
    2.IE盒模型
        width包括content尺寸＋padding+border
$\textcolor{red}{谈一谈你对BFC/IFC的理解}$需要学习

## 多行截断
这个截断两行
```css
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    overflow: hidden;
    text-overflow: ellipsis;
```
## 单行文本截断
```css
-webkit-box-orient: vertical;
overflow: hidden;
white-space: nowrap; // 多行状态下不换行
text-overflow: ellipsis;
```


## JS原型链
- 构造函数:类似于Struct 要配合new关键字来使用
- 构造函数原型prototype(原型对象):原型对象的所有属性和方法都会被构造函数所拥有, 并且这些属性和方法是被共享的，不需要开辟空间
- 对象原型__proto__:创建的实例对象在添加一个__proto__指向到构造函数原型prototype
- constructor构造函数:用于指向构造函数
- 写出一个构造函数Star之后 用Star声明一个一个对象实例s之后，那么构造函数上面会带上一个prototype用来继承于从Object那里
继承过来的方法和属性，这个类似于类，同时这个prototype原型对象上面由一个constructor用来指向Star表示由Star创建，而对象实例s
上面有一个__proto__用来指向Star原型对象prototype,也就继承了prototype上面的方法和属性

## Python forms.Form 表单验证
导入
```python
from django import forms
class SelfForm(forms.Form):
    pass
```
自定义类里面的元素一般为表单传递过来的对应参数
```python
class SelfForm(forms.Form):
    #  只会验证是否为空,即每个Field类的值都是必要的，可设置required=False来取消 可通过error_messages={'required': 'msg'}自定义报错信息
    name=forms.CharFiled()
    def clean(self):
        pass    # 验证数据 里面写入对应方法

    def clean_name(self):
    # 遇到错误之后可以通过Form(self).add_error(field,error)来添加错误到特定字段
    # field 参数是应该添加错误的字段名
    # error 参数可以是一个字符串
    # Form.add_error() 会自动从 cleaned_data 中删除相关字段
        pass        # 用于验证指定的字段 并且都是clean方法在之前执行， clean_方法在后面
```
在这里面有自带有一个self.cleaned_data, 这个里面只会加入验证通过的字段

使用
```python
form = SelfForm(data) # 传入要验证的数据
form.is_valid() #调用验证 并且返回一个布尔值，如果数据有效就返回true，无效就返回false
form.errors # 里面放有所有错误信息的字典 格式：{'name':'error_messages'}
```

## CSS
p:first-child nth-child last-child
指的是对于某一个父元素来说，其第一个子元素如果是p的话就是对其操作,同理可知剩下两个