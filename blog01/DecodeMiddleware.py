from django.utils.deprecation import MiddlewareMixin
import json


# 解析post请求的数据
class MD1(MiddlewareMixin):
    # 请求中间件
    def process_request(self, request):
        # 用来解析json格式的数据
        if request.method != 'GET' and request.META.get('CONTENT_TYPE') == 'application/json':
            print('提取json数据放入到request.data中')
            data = json.loads(request.body, encoding='utf-8')
            request.data = data

    def process_response(self, request, response):
        return response