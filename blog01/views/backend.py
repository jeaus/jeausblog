from django.shortcuts import render, redirect

from blog01.models import *
from blogapi.utils.permissions_control import is_super_fun




# Create your views here.
def backend(request):
    if not request.user.username:
        # 没有登录
        return redirect('/')
    user = request.user
    collect_query = user.collects.all()
    return render(request, 'backend/backend.html', locals())


@is_super_fun
def add_article(request):
    # 拿到所有的分类，标签
    tag_list = Tags.objects.all()
    # 拿到所有的文章封面
    cover_list = Cover.objects.all()
    category_list = Articles.category_choice
    print(category_list)
    return render(request, 'backend/add_article.html', locals())


# 编辑头像
def change_avatar(request):
    avatar_url = request.user.avatar.url.url
    avatar_list = Avatars.objects.all()
    for i in avatar_list:
        if i.url.url == avatar_url:
            avatar_id = i.nid
    return render(request, 'backend/change_avator.html', locals())


def reset_pwd(request):
    return render(request, 'backend/reset_pwd.html', locals())


@is_super_fun
def edit_article(request, nid):
    article_obj = Articles.objects.get(nid=nid)  # 获取到对应的文章
    tags = [str(tag.nid) for tag in article_obj.tag.all()]  # 拿到已经选择的标签
    tag_list = Tags.objects.all()
    cover_list = Cover.objects.all()  # 获取所有的封面
    category_list = Articles.category_choice
    return render(request, 'backend/edit_article.html', locals())


@is_super_fun
def admin_home(request):
    return render(request, 'backend/admin/admin_home.html')


# 头像列表
@is_super_fun
def avatar_list(request):
    avatar_all = Avatars.objects.all()
    return render(request, 'backend/avatar_list.html', locals())


# 文章封面
@is_super_fun
def cover_list(request):
    cover_query = Cover.objects.all()
    return render(request, 'backend/cover_list.html', locals())
