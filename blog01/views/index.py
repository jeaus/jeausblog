from pprint import pprint

from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.contrib import auth
from blog01.models import *
from django.db.models import F, Q

from blog01.utils.verification import random_verify_code
from blog01.utils.subcomment import sub_comment_list
from blog01.utils.pagination import Pagination


# Create your views here.
def index(request):
    # 已经排序好的文章
    article_list = Articles.objects.filter(status=1).order_by('-change_date')
    # 这两个不用分页器处理
    front_list = article_list.filter(category=1)[:4]
    backend_list = article_list.filter(category=2)[:4]
    # 这个是查询的条件 是字典
    query_params = request.GET.copy()
    # 用分页器实现
    page = Pagination(
        current_page=int(request.GET.get('page', '1')),
        all_count=article_list.count(),
        base_url=request.path_info,
        query_params=query_params,
        per_page=5,
        pager_page_count=5
    )
    article_list = article_list[page.start:page.end]
    advert_list = Advert.objects.filter(is_show=True)
    cover_list = Cover.objects.all()
    # 获取所有的tag
    tags = Tags.objects.exclude(articles__isnull=True)
    return render(request, 'blog01/index.html', locals())


def search(request):
    search_key = request.GET.get('key', '')
    order = request.GET.get('order', '-create_date')
    word = request.GET.getlist('word')
    tag = request.GET.get('tag', '')
    article_list = Articles.objects.filter(Q(title__contains=search_key) | Q(tag__title__contains=tag))
    query_params = request.GET.copy()
    if len(word) == 2:
        article_list = article_list.filter(word__range=word)
    if tag:
        article_list = article_list.filter(tag__title=tag)

    if order:
        try:
            article_list = article_list.order_by(order)
            pprint(article_list)
        except Exception:
            order = '-change_date'
    else:
        order = '-change_date'
    page = Pagination(
        current_page=int(request.GET.get('page', '1')),
        all_count=article_list.count(),
        base_url=request.path_info,
        query_params=query_params,
        per_page=5,
        pager_page_count=7
    )
    article_list = article_list[page.start:page.end]
    # 文章搜索条件
    return render(request, 'blog01/search.html', locals())


def article(request, nid=1):
    article_query = Articles.objects.filter(nid=nid)
    if not article_query:
        return redirect('/')
    article_obj = article_query.first()
    if article_obj.pwd:
        # 获取session里面的数据 判断是否已经输入过密码
        pwd = request.session.get(f'article_pwd__{nid}')
        if pwd != article_obj.pwd:
            return render(request, 'blog01/article_lock.html', locals())
    article_query.update(look_count=F('look_count') + 1)
    comment_list = sub_comment_list(nid)
    return render(request, 'blog01/article.html', locals())


def test(request):
    return render(request, 'blog01/test.html', {'request': request})


# 退出登录函数
def logout(request):
    auth.logout(request)
    return redirect('/')


# 获取随机验证码
def verify_code(request):
    data, verifycode = random_verify_code()
    request.session['verify_code'] = verifycode
    request.session['error_count'] = 0
    return HttpResponse(data)


# 心情页面
def moods(request):
    # 查询所有的头像
    avatar_list = Avatars.objects.all()
    # 搜索
    key = request.GET.get('key', '')
    mood_list = Moods.objects.filter(content__contains=key).order_by('-create_date')
    query_params = request.GET.copy()
    page = Pagination(
        current_page=int(request.GET.get('page', '1')),
        all_count=mood_list.count(),
        base_url=request.path_info,
        query_params=query_params,
        per_page=3,
        pager_page_count=7
    )
    mood_list = mood_list[page.start:page.end]
    return render(request, 'blog01/moods.html', locals())


# 回忆录
def history(request):
    history_list = History.objects.all().order_by('-create_date')
    return render(request, 'blog01/history.html', locals())


# 网站页面
def site(request):
    # 取所有的标签
    tag_all = NavTags.objects.all()
    tag_list = tag_all.exclude(navs__isnull=True)
    return render(request, 'blog01/site.html', locals())


# 网站关于页面
def about(request):
    return render(request, 'blog01/about.html', locals())
