from django import template
from blog01.utils.search import Search
from django.utils.safestring import mark_safe
from blog01.models import Tags, Avatars, Menu, Articles

register = template.Library()


# 自定义过滤器
# @register.filter
# def add1(item):
#     return int(item)+1


@register.inclusion_tag('blog01/my_tag/headers.html')
def banner(menu_name, article=None):
    if article:
        # 说明是文章详情
        # 拿到文章的封面
        cover = article.cover.url.url
        img_list = [cover]
        menu_time = 0
        title = article.title
        return locals()
    menu_obj = Menu.objects.get(menu_title_en=menu_name)
    menu_time = menu_obj.menu_time  # 轮播时间
    title = menu_obj.title
    if menu_obj.menu_rotation:
        img_list = [
            i.url.url
            for i in menu_obj.menu_url.all()
        ]
    else:
        # 不轮播
        img_list = [menu_obj.menu_url.all().first().url.url]
        menu_time = 0

    return locals()


@register.simple_tag
def generate_order_html(request, key):
    order_list = []
    order = ''  # 用于设置选择排序方式
    if key == 'order':
        order = request.GET.get('order', '-create_date')
        order_list = [
            ('-create_date', '综合排序'),
            ('-change_date', '最新发布'),
            ('-collects_count', '最多收藏'),
            ('-comment_count', '最多评论'),
            ('-look_count', '最多浏览'),
        ]
    elif key == 'word':
        order = request.GET.getlist('word', '')
        order_list = [
            ('', '全部字数'),
            (['0', '100'], '100字'),
            (['100', '500'], '500字'),
            (['500', '1000'], '1000字'),
            (['1000', '3000'], '3000字'),
            (['3000', '5000'], '5000字'),
        ]
    elif key == 'tag':
        order = request.GET.get('tag', '')
        tag_list = Tags.objects.exclude(articles__isnull=True)
        order_list.append(('', '全部标签'))
        for tag in tag_list:
            order_list.append((tag.title, tag.title))
    query_params = request.GET.copy()
    order = Search(
        current_order=order,
        order_list=order_list,
        query_params=query_params,
        key=key
    )

    return mark_safe(order.order_html())


# 生成动态导航条
@register.simple_tag
def dynamic_nav(request):
    path = request.path_info
    path_dict = {
        '/': '首页',
        '/moods': '心情',
        '/history': '回忆录',
        '/about': '关于',
        '/sites': '网站导航',
    }
    nav_list = []
    for k, v in path_dict.items():
        if k == path:
            nav_list.append(f'<a href="{k}" class="active">{v}</a>')
        else:
            nav_list.append(f'<a href="{k}">{v}</a>')
    return mark_safe(''.join(nav_list))


#  生成广告
@register.simple_tag
def generate_advert(advert_list):
    html_list = []
    for i in advert_list:
        if i.img:
            # 上传的文件
            html_list.append(
                f'<div><a href="{i.href}" title="{i.title}" target="_blank"><img src="{i.img.url}"></a></div>')
        else:
            html_s: str = i.img_list
            html_new = html_s.replace('；', ';').replace('\r\n', '')
            img_list = html_new.split(';')
            for url in img_list:
                html_list.append(
                    f'<div><a href="{i.href}" title="{i.title}" target="_blank"><img src="{url}"></a></div>')
    return mark_safe(''.join(html_list))


@register.simple_tag
def generate_drawing(draw: str):
    if not draw:
        return ''
    html_new = draw.replace('；', ';').replace('\r\n', ';').replace('\n', ';')
    img_list = html_new.split(';')
    html_str = ''
    for i in img_list:
        html_str += f'<img src="{i}" alt="" @error="img_error">'
    return mark_safe(html_str)


@register.simple_tag
def generate_content(content):
    if not content:
        return ''
    html_new = content.replace('；', ';').replace('\r\n', ';').replace('\n', ';')
    img_list = html_new.split(';')
    html_str = ''
    for i in img_list:
        html_str += f'<li>{i}</li>'
    return mark_safe(html_str)


# 上一篇 下一篇
@register.simple_tag
def generate_p_n(article: Articles):

    article_list = list(Articles.objects.filter(category=article.category))
    now_index = article_list.index(article)
    max_index = len(article_list)-1
    # 上一篇
    if now_index == 0:
        prev = '<a href="javascript:void(0);">已经是第一篇了</a>'
    else:
        prev_article = article_list[article_list.index(article)-1]
        prev = f'<a href="/article/{prev_article.nid}">上一篇:{prev_article.title}</a>'
    if now_index == max_index:
        next = '<a href="javascript:void(0);">已经是最后一篇了</a>'
    else:
        # 下一篇
        next_article = article_list[article_list.index(article)+1]
        next = f'<a href="/article/{next_article.nid}">下一篇:{next_article.title}</a>'
    return mark_safe(prev+next)


# 计算某个分类的文章数
@register.simple_tag
def calculation_category_count(cid):
    article_query = Articles.objects.filter(category=cid)
    return article_query.count()
